library verilog;
use verilog.vl_types.all;
entity LFSR is
    port(
        load_enable     : in     vl_logic;
        clk             : in     vl_logic;
        q_out           : out    vl_logic
    );
end LFSR;
