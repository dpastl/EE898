# README #

This code is provided for the purpose of sharing information.  There is no warranty or guarantee that it will work.  Please feel free to use as you please and if you find it useful, I'd love to hear about it.


### DOCSIS 3.1 Simplified Transmitter ###

This is a simplified transmitter, only performing QPSK transmission on all 256 channels.  This project was designed to run on a Altera Stratix FPGA but was never tested on hardware due to limitations in accessing this hardware.  Instead the included modesim projects were used to debug the project.  There are three folders that are of interest.  The first is LFSR which is a separate project that creates pseudo random numbers necessary for the code to run.  If the seed is the same you will always get the same sequence.  The next is the OFDM_tx code, which creates the transmitter.  Finally the RCNT folder is a special counter to keep track of the symbols as they are generated.  This function is a separate project because it could be reused in the receiver.

### How do I get set up? ###

* Summary of set up
To get going you will first need the Altera Quartus tools available from http://www.altera.com/products/software/quartus-ii/subscription-edition/qts-se-index.html.  You can use the free edition software but you likely will not be able to use the modelsim files and certainly cannot compile for the Stratix FPGAs
* Configuration
Should mostly be set up in the OFDM_tx project
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact