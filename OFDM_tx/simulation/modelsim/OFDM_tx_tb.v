`timescale 1 ns / 1 ps 
module OFDM_tx_tb();
  reg clk, reset, areset;
  wire sym_clk, sink_sop, sink_eop, source_sop, source_eop, source_valid, source_ready, tx_clock;
  wire [1:0] source_error, sink_error;
  wire [7:0] sym_count;
  wire [8:0] ram_read_addr, ram_write_addr, ram_read_counter;
  wire [17:0] sink_real, sink_imag, source_real, source_imag, ifft_imag, ifft_real;
  wire sink_ready;
  
  initial
  begin
    clk = 0;
    reset = 0;
  end
  
  initial
    #150 reset = 1;
  
  initial
    //#6000 $stop; // stop the simulation at 150 ns
    #50000 $stop; // stop the simulation at 150 ns
  
  always
    #10 clk =! clk;

  
  OFDM_tx OFDM_tx1(
                  .clk            (clk),
                  .reset          (reset),
                  .sym_clk        (sym_clk),
                  .tx_clock       (tx_clock),
                  .sym_count_out  (sym_count),
                  .sink_real      (sink_real),
                  .sink_imag      (sink_imag),
                  .sink_sop       (sink_sop),
                  .sink_eop       (sink_eop),
                  .source_real    (source_real),
                  .source_imag    (source_imag),
                  .source_sop     (source_sop),
                  .source_eop     (source_eop),
                  .source_valid   (source_valid),
                  .source_ready   (source_ready),
                  .sink_error     (sink_error),
                  .source_error   (source_error),
                  .sink_ready     (sink_ready),
                  .ram_read_addr  (ram_read_addr),
                  .ram_write_addr (ram_write_addr),
                  .ifft_imag      (ifft_imag),
                  .ifft_real      (ifft_real),
                  .ram_read_counter(ram_read_counter)
                  );
endmodule
